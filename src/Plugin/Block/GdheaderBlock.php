<?php

namespace Drupal\gd_header\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Block\TitleBlockPluginInterface;
use Drupal\Component\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with a responsive image as background and page title.
 *
 * @Block(
 *   id = "gd_header_block",
 *   context = {
 *    "node" = @ContextDefinition("entity:node", required = FALSE),
 *    "taxonomy_term" = @ContextDefinition("entity:taxonomy_term", required = FALSE)
 *   },
 *   admin_label = @Translation("GD Header"),
 *   category = @Translation("Theme"),
 * )
 */
class GdheaderBlock extends BlockBase implements TitleBlockPluginInterface, BlockPluginInterface, ContainerFactoryPluginInterface, ContextAwarePluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageBase
   */
  protected $fileUsage;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The page title: a string (plain title) or a render array (formatted title).
   *
   * @var string|array
   */
  protected $title = '';

  /**
   * Construct.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\file\FileUsage\FileUsageInterface $file_usage
   *   The file usage service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, EntityTypeManagerInterface $entity_type_manager, FileUsageInterface $file_usage, ImageFactory $image_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUsage = $file_usage;
    $this->imageFactory = $image_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('file.usage'),
      $container->get('image.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form = parent::blockForm($form, $form_state);

    $fieldOptions = [];
    foreach ($this->entityTypeManager->getStorage('field_storage_config')->loadMultiple() as $field) {
      if ($field->getType() == 'image') {
        $fieldOptions[$field->getName()] = $field->getName();
      }
    }
    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#description' => $this->t('Choose an image field.'),
      '#options' => $fieldOptions,
      '#default_value' => $config['field'] ?? '',
    ];

    $styleOptions = [];
    foreach ($this->entityTypeManager->getStorage('responsive_image_style')->loadMultiple() as $style) {
      $styleOptions[$style->id()] = $style->label();
    }
    $form['responsive_image_style_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Responsive image style'),
      '#description' => $this->t('Choose a responsive image style.'),
      '#options' => $styleOptions,
      '#default_value' => $config['responsive_image_style_id'] ?? '',
    ];

    $form['images'] = [
      '#type' => 'managed_file',
      '#name' => 'images',
      '#default_value' => $config['images'] ?? '',
      '#title' => $this->t('Default images'),
      '#description' => $this->t('An image will be randomly chosen in this default selection when no field is available.'),
      '#upload_validators' => [
        'file_validate_extensions' => [
          'jpg jpeg png',
        ],
      ],
      '#upload_location' => 'public://gdheader/',
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['field'] = $values['field'];
    $this->configuration['responsive_image_style_id'] = $values['responsive_image_style_id'];
    $this->configuration['images'] = $values['images'];

    $images = $values['images'];
    if (isset($images)) {
      foreach ($images as $image) {
        $file = File::load($image);
        if ($file) {
          if (!($file->isPermanent())) {
            $file->setPermanent();
            $file->save();
          }
          $fileUsage = $this->fileUsage->listUsage($file);
          if (
            isset($fileUsage['gd_header']) &&
            isset($fileUsage['gd_header']['block']) &&
            isset($fileUsage['gd_header']['block']['gdheader'])
          ) {
            continue;
          }
          else {
            $this->fileUsage->add($file, 'gd_header', 'block', 'gdheader');
          }
        }
      }
    }
    $filesUsages = $this->connection
      ->select('file_usage', 'u')
      ->fields('u')
      ->condition('u.module', 'gd_header', '=')
      ->condition('u.type', 'block', '=')
      ->condition('u.id', 'gdheader', '=')
      ->execute();
    $fidsUsagesToDelete = [];
    while ($fileUsage = $filesUsages->fetchAssoc()) {
      if (isset($images)) {
        if (in_array($fileUsage['fid'], $images)) {
          continue;
        }
        else {
          $fidsUsagesToDelete[] = $fileUsage['fid'];
        }
      }
      else {
        $fidsUsagesToDelete[] = $fileUsage['fid'];
      }
    }
    foreach ($fidsUsagesToDelete as $fidUsageToDelete) {
      $fileToDelete = File::load($fidUsageToDelete);
      $this->fileUsage->delete($fileToDelete, 'gd_header', 'block', 'gdheader');
      $fileToDelete->setTemporary();
      $fileToDelete->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (empty($style = $config['responsive_image_style_id'])) {
      return;
    }

    $node = $this->getContextValue('node');
    $term = $this->getContextValue('taxonomy_term');
    if (
      $node &&
      !empty($field = $config['field']) &&
      $node->hasField($field) &&
      !empty($imageReference = $node->get($field)->first())
    ) {
      $file = $imageReference->get('entity')->getTarget()->getValue();
    }
    elseif (
      $term &&
      !empty($field = $config['field']) &&
      $term->hasField($field) &&
      !empty($imageReference = $term->get($field)->first())
    ) {
      $file = $imageReference->get('entity')->getTarget()->getValue();
    }
    elseif (empty($config['images'])) {
      return;
    }
    else {
      $images = $config['images'];
      $i = rand(0, count($images) - 1);
      $file = File::load($images[$i]);
    }
    $uri = $file->getFileUri();
    $img = $this->imageFactory->get($uri);
    $buildImage = [
      '#theme' => 'responsive_image',
      '#uri' => $uri,
      '#attributes' => [
        'alt' => isset($imageReference) ? $imageReference->alt : '',
        'title' => isset($imageReference) ? $imageReference->title : '',
      ],
      '#responsive_image_style_id' => $style,
      '#height' => $img->getHeight(),
      '#width' => $img->getWidth(),
    ];

    return [
      'title' => [
        '#type' => 'page_title',
        '#title' => $this->title,
      ],
      'image' => $buildImage ?? NULL,
    ];
  }

}
