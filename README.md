CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The GD Header module provides a block with the page title and a responsive image.

The responsive image is taken from a selected field for nodes and taxonomy terms. If the field is not available, and in other pages, then an image will be randomly chosen in a default selection of images that you can manage.

A live example can be seen on the [developer website](https://guillaumeduveau.com/en/drupal/developer).

*Please note that this module is meant to be used with some basic theming on your side, especially parallax implementation.*

 * For a full description of the module visit:
   https://www.drupal.org/project/gd_header

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/gd_header


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the GD Header module as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Media > Image styles and add some image styles.
    3. Navigate to Administration > Configuration > Media > Responsive image styles and add some responsive image styles.
    4. Navigate to Administration > Structure > Content types > [Content type to edit] and add at least one image field.
    5. Navigate to Administration > Structure > Taxonomy > [Vocabulary to edit] and add at least one image field (with the same name).
    5. Navigate to Administration > Structure > Block layout > Place block in the appropriate region, select the GD Header block and configure it (see below).

BLOCK CONFIGURATION
-------------------

    1. Select Node from URL.
    2. Select Taxonomy term from URL.
    3. Select a field to use for the responsive image.
    4. Select the responsive image style.
    5. Add default images

CUSTOMIZATION
-------------

Copy your base block.html.twig into your theme template and rename it to block--gdheader.html.twig (or other theme suggestion).

Example with [MaterializeCSS Parallax](https://materializecss.com/parallax.html):

````
<div class="parallax-container center valign-wrapper">
  {% block content %}
    <div class="parallax">
      {{ content.image }}
    </div>
    {{ content.title }}
  {% endblock %}
</div>
````

MAINTAINERS
-----------

 * Guillaume Duveau - https://www.drupal.org/user/173213
